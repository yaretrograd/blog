<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testUserCanCreatePost(): void
    {
        $user = User::factory()->create();
        $data = [
            'title' => 'Test Post',
            'detail_text' => 'This is a test post',
            'image' => UploadedFile::fake()->image('test.jpg', 200, 200),
        ];
        $this->actingAs($user)->postJson(route('posts.store'), $data);

        $this->assertDatabaseHas('posts', [
            'title' => $data['title'],
            'detail_text' => $data['detail_text'],
            'user_id' => $user->id,
        ]);
    }

    public function testUserCanUpdateOwnPost()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $data = [
            'title' => 'Test Post',
            'image' => UploadedFile::fake()->image('test.jpg', 200, 200),
        ];
        $this->actingAs($user)->putJson(route('posts.update', $post->id), $data);
        $this->assertDatabaseHas('posts', [
            'title' => $data['title'],
        ]);
    }

    public function testUserCannotUpdatePostBelongingToAnotherUser()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $post = Post::factory()->for($user1)->create();
        $data = [
            'title' => 'Test Post',
        ];
        $response = $this->actingAs($user2)->putJson(route('posts.update', $post->id), $data);
        $response->assertStatus(403);
    }

    public function testUserCannotDeletePost()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $response = $this->actingAs($user)->deleteJson(route('posts.destroy', $post->id));
        $response->assertStatus(403);
    }

    public function testUserCannotUpdatePostApprovedStatus()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();

        $response = $this->actingAs($user)
            ->postJson(route('posts.update-approved', $post->id), ['is_approved' => false]);
        $response->assertStatus(403);
    }

    public function testAdminCanDeletePost()
    {
        $admin = User::factory()->state(['role' => 'admin'])->create();
        $post = Post::factory()->for($admin)->create();
        $response = $this->actingAs($admin)->deleteJson(route('posts.destroy', $post->id));
        $response->assertStatus(204);
    }

    public function testAdminCanUpdatePostApprovedStatus()
    {
        $admin = User::factory()->state(['role' => 'admin'])->create();
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();

        $this->actingAs($admin)
            ->postJson(route('posts.update-approved', $post->id), ['is_approved' => true]);
        $this->assertEquals(true, $post->fresh()->is_approved);

        $this->actingAs($admin)
            ->postJson(route('posts.update-approved', $post->id), ['is_approved' => false]);
        $this->assertEquals(false, $post->fresh()->is_approved);
    }
}
