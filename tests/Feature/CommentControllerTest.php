<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->setUpFaker();
    }

    public function testAllUsersCanCreateComment()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $data = [
            'name'    => $this->faker->name(),
            'email'   => $this->faker->email(),
            'text'    => $this->faker->sentence(15),
            'post_id' => $post->id,
        ];
        $this->postJson(route('comments.store'), $data);
        $this->assertDatabaseHas('comments', $data);
    }

    public function testGuestAndUserCannotUpdateComment()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->for($post)->create();
        $data     = [
            'text' => $this->faker->sentence(),
        ];

        $responseForGuest = $this->putJson(route('comments.update', $comment->id), $data);
        $responseForGuest->assertStatus(401);

        $responseForUser = $this->actingAs($user)->putJson(route('comments.update', $comment->id), $data);
        $responseForUser->assertStatus(403);
    }

    public function testGuestAndUserCannotDeleteComment()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->for($post)->create();

        $responseForGuest = $this->deleteJson(route('comments.destroy', $comment->id));
        $responseForGuest->assertStatus(401);

        $responseForUser = $this->actingAs($user)->deleteJson(route('comments.destroy', $comment->id));
        $responseForUser->assertStatus(403);
    }

    public function testGuestCannotUpdateApproved()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->state(['is_approved' => false])->for($post)->create();
        $data = ['is_approved' => true];

        $responseForGuest = $this->postJson(route('comment.update-approved', $comment->id), $data);
        $responseForGuest->assertStatus(401);
    }

    public function testUserCanUpdateApprovedOnce()
    {
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->state(['is_approved' => false])->for($post)->create();
        $data = ['is_approved' => true];

        $response = $this->actingAs($user)
            ->postJson(route('comment.update-approved', $comment->id), ['is_approved' => true]);
        $response->assertStatus(200);

        $response = $this->actingAs($user)
            ->postJson(route('comment.update-approved', $comment->id), ['is_approved' => false]);
        $response->assertStatus(403);
    }

    public function testAdminCanUpdateComment()
    {
        $admin = User::factory()->state(['role' => 'admin'])->create();
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->for($post)->create();

        $data     = [
            'text' => $this->faker->sentence(),
        ];

        $this->actingAs($admin)->putJson(route('comments.update', $comment->id), $data);
        $this->assertEquals($data['text'], $comment->fresh()->text);
    }

    public function testAdminCanDeleteComment()
    {
        $admin = User::factory()->state(['role' => 'admin'])->create();
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->for($post)->create();

        $response = $this->actingAs($admin)->deleteJson(route('comments.destroy', $comment->id));
        $response->assertStatus(204);
        $this->assertDatabaseMissing('comments', $comment->toArray());
    }

    public function testAdminCanUpdateApproved()
    {
        $admin = User::factory()->state(['role' => 'admin'])->create();
        $user = User::factory()->create();
        $post = Post::factory()->for($user)->create();
        $comment = Comment::factory()->state(['is_approved' => false])->for($post)->create();
        $data = ['is_approved' => true];

        $this->actingAs($admin)->postJson(route('comment.update-approved', $comment->id), $data);
        $this->assertEquals(true, $comment->fresh()->is_approved);
    }
}
