<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->setUpFaker();
    }

    public function testGuestRegisterSuccess()
    {
        $data     = [
            'name'     => $this->faker->name(),
            'email'    => $this->faker->email(),
            'password' => $this->faker->password(6, 40),
        ];
        $response = $this->postJson(route('auth.register'), $data);
        $response->assertStatus(200)->assertJson(['token_type' => 'bearer']);
    }

    public function testUserAuthenticateSuccess()
    {
        $name     = $this->faker->name();
        $email    = $this->faker->email();
        $password = $this->faker->password(6, 40);
        User::factory()->create([
            'name'     => $name,
            'email'    => $email,
            'password' => bcrypt($password),
        ]);
        $response = $this->postJson(route('auth.login'), ['email' => $email, 'password' => $password]);
        $response->assertStatus(200)->assertJson(['token_type' => 'bearer']);
    }

    public function testAuthenticatedUserCanRetrieveTheirInformation()
    {
        $user  = User::factory()->create([
            'name'     => $name = $this->faker->name(),
            'email'    => $email = $this->faker->email(),
            'password' => bcrypt($this->faker->password(6, 40)),
        ]);
        $token = auth()->login($user);

        $response = $this->withHeaders([
            'Authorization' => "Bearer $token",
        ])->postJson(route('auth.me'));
        $response->assertStatus(200)->assertJson([
            'name'  => $name,
            'email' => $email
        ]);
    }

    public function testAuthenticatedUserCanLogout()
    {
        $user  = User::factory()->create([
            'name'     => $name = $this->faker->name(),
            'email'    => $email = $this->faker->email(),
            'password' => bcrypt($this->faker->password(6, 40)),
        ]);
        $token = auth()->login($user);

        $response = $this->withHeaders([
            'Authorization' => "Bearer $token",
        ])->postJson(route('auth.logout'));
        $response->assertStatus(200)->assertJson([
            'message' => 'Successfully logged out'
        ]);
    }
}
