<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::apiResource('posts', PostController::class);
Route::post('/posts/{post}/update_approved', [PostController::class, 'updateApproved'])
    ->name('posts.update-approved');

Route::apiResource('/comments', CommentController::class);
Route::post('/comments/{comment}/update_approved', [CommentController::class, 'updateApproved'])
    ->name('comment.update-approved');

Route::controller(AuthController::class)
    ->prefix('auth')
    ->name('auth.')
    ->group(function () {
        Route::post('register', 'register')->name('register');
        Route::post('login', 'login')->name('login');
        Route::post('logout', 'logout')->name('logout');
        Route::post('refresh', 'refresh')->name('refresh');
        Route::post('me', 'me')->name('me');
    });
