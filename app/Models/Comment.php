<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasFactory;

    protected $casts = [
        'is_approved' => 'boolean',
    ];

    protected $fillable = [
        'name',
        'email',
        'is_approved',
        'text'
    ];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function scopeApproved(Builder $query): Builder
    {
        return $query->where('is_approved', 1);
    }

    public function scopeBlocked(Builder $query): Builder
    {
        return $query->where('is_approved', 0);
    }
}
