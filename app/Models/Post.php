<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    protected $casts = [
        'is_approved' => 'boolean',
    ];

    protected $fillable = [
        'title',
        'slug',
        'detail_text',
        'image',
    ];

    protected $appends = ['preview_text'];

    public function getPreviewTextAttribute(): string
    {
        return substr($this->detail_text, 0, 40) . '...';
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
