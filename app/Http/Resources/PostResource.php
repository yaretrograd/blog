<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'           => $this->id,
            'user_id'      => $this->user_id,
            'user'         => (new UserResource($this->whenLoaded('user'))),
            'title'        => $this->title,
            'slug'         => $this->slug,
            'is_approved'  => $this->is_approved,
            'detail_text'  => $this->detail_text,
            'preview_text' => $this->preview_text,
            'image'        => $this->image,
            'comments'     => (CommentResource::collection($this->whenLoaded('comments'))),
        ];
    }
}
