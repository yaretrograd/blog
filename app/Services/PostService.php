<?php

namespace App\Services;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostApprovalRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostService
{
    public function indexPosts(): array|\Illuminate\Database\Eloquent\Collection
    {
        return Post::with(['comments' => function ($query) {
            $query->approved();
        }])->where('is_approved', true)->get();
    }

    public function createPost(StorePostRequest $request): Post
    {
        $data = $request->validated();
        $data['slug'] = Str::slug($data['title']);

        $data['image'] = $this->storeImage($request->file('image'));

        $user = auth()->user();

        $post = Post::make($data);
        $post->user()->associate($user);
        $post->save();

        return $post;
    }

    public function updatePost(UpdatePostRequest $request, Post $post): Post
    {
        $data = $request->validated();
        if(!empty($data['title']) && $data['title'] !== $post->title) {
            $data['slug'] = Str::slug($data['title']);
        }
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $this->deleteImageFromStorage($post->image);
            $data['image'] = $this->storeImage($file);
        }
        $post->update($data);
        return $post;
    }

    public function deletePost(Post $post): void
    {
        $post->delete();
    }

    public function changePostApprovedStatus(Post $post, UpdatePostApprovalRequest $request): void
    {
        $post->is_approved = $request->is_approved;
        $post->save();
    }

    private function storeImage(UploadedFile $image): string
    {
        $extension = $image->getClientOriginalExtension();
        $filename = Str::random(25) . '.' . $extension;
        return Storage::putFileAs('posts', $image, $filename);
    }

    private function deleteImageFromStorage($filename): void
    {
        if(Storage::exists($filename)) {
            Storage::delete($filename);
        }
    }
}
