<?php

namespace App\Services;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentApprovalRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Post;

class CommentService
{
    public function createComment(StoreCommentRequest $request): Comment
    {
        $data = $request->validated();
        $post = Post::find($data['post_id']);
        return $post->comments()->create($data);
    }

    public function updateComment(UpdateCommentRequest $request, Comment $comment): Comment
    {
        $data = $request->validated();
        $comment->update($data);
        return $comment;
    }

    public function deleteComment(Comment $comment): void
    {
        $comment->delete();
    }

    public function changeCommentApprovedStatus(Comment $comment, UpdateCommentApprovalRequest $request): void
    {
        $comment->is_approved = $request->is_approved;
        $comment->save();
    }
}
